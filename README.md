# Технический прототип
## Экран
Две камеры в одной точке с разным FOV и небольшой Y дельтой (считаем что глаза и прецел находятся на разных уровнях):
#### 1. ViewCamera
##### Settings
```
FOV = 60;    
Viewport Rect->Y = 0.5    
```
##### Postprocessing

    по необходимости и запасу по производительности

### 2. AimViewCamera
##### Settings

```
parented to ViewCamera;    
transform.localPosition.y = parent.transform.localPosition + deltaY;    
FOV = 10;   
Viewport Rect->Y = -0.5   
```
##### Postprocessing
- *texture overlay*    
    stencil buffer - отсекает часть пикселей, которая как бы закрыта прицелом.  
    texture overlay - непосредственно текстура, которая будет определять стиль прицела с различными метками и тд.

- *lens distortion*    
    в postporicessing 2.0 есть данный эффект, но он слишком базовый, поэтому могу предположить, что надо будет писать отдельный шейдер.

- *other*    
    по необходимости и запасу по производительности

![Alt text] (https://bitbucket.org/q23w1e/safari/raw/a38cdfe30e1ec6c73b78128c9f0f1e090baf1af5/resourcesmd/prototype.gif)

---


## Окружение
### 1. Модели с LOD.     
Позволит экономить на отрисовке в случае ViewCamera, т.к. определённая часть объектов будет вегда слишком далеко для "детальной" детализации.
### 2. ...